// Header mobile

let burger = document.querySelector(".burger-container");
let header = document.querySelector(".header");
let html = document.querySelector("html");

if (burger) {
  burger.addEventListener("click", () => {
    header.classList.toggle("header_menu-opened");
    html.classList.toggle("overflow-hidden");
  });
}

let headerTabBg = document.querySelector(".header__tab-bg");
let headerTab = document.querySelectorAll(".header__tab");
let headerMenuItem = document.querySelectorAll(".header__menu-item");
let headerTabWrapper = document.querySelectorAll(".header__tab-wrapper");

// Header tabs
if (screen.width > 1150) {
  headerMenuItem.forEach((item) => {
    let itemHeaderTabWrapper = item.querySelector(".header__tab-wrapper");
    let headerTab = item.querySelector(".header__tab");
    if (itemHeaderTabWrapper) {
      item.addEventListener("mouseover", () => {
        headerTabBg.classList.add("header__tab-bg_active");
        headerTab.classList.add("header__tab_active");
        itemHeaderTabWrapper.classList.add("header__tab-wrapper_active");
      });
      item.addEventListener("mouseout", () => {
        headerTabBg.classList.remove("header__tab-bg_active");
        headerTabBg.classList.remove("header__tab-bg_hovered");
        headerTab.classList.remove("header__tab_active");
        itemHeaderTabWrapper.classList.remove("header__tab-wrapper_active");
      });
    }
  });

  document.addEventListener("click", (e) => {
    if (e.target.classList.contains("header__tab-wrapper") || e.target.classList.contains("header__tab-list")) {
      headerTabBg.classList.remove("header__tab-bg_active");
      headerTabBg.classList.remove("header__tab-bg_hovered");
      headerTab.forEach((tab) => {
        tab.classList.remove("header__tab_active");
      });
      headerTabWrapper.forEach((wrapper) => {
        wrapper.classList.remove("header__tab-wrapper_active");
      });
    }
  });

  headerTabWrapper.forEach((tabWrapper) => {
    tabWrapper.addEventListener("mouseover", () => {
      headerTabBg.classList.add("header__tab-bg_hovered");
    });
  });
} else {
  let headerMenuLink = document.querySelectorAll(".header__menu-link");
  let headerTab = document.querySelectorAll(".header__tab");
  let headerWrapper = document.querySelectorAll(".header__tab-wrapper");

  headerMenuLink.forEach((link, i) => {
    link.addEventListener("click", (e) => {
      link.classList.toggle("header__menu-link_active");
      headerMenuLink.forEach((item, j) => {
        if (j != i) {
          item.classList.remove("header__menu-link_active");
        }
      });
      if (link.dataset.tab) {
        e.preventDefault();
        headerWrapper.forEach((wrapper, m) => {
          if (m == Number(link.dataset.tab)) {
            wrapper.classList.toggle("header__tab-wrapper_active");
          } else {
            wrapper.classList.remove("header__tab-wrapper_active");
          }
        });
        headerTab.forEach((tab, k) => {
          if (k == Number(link.dataset.tab)) {
            tab.classList.toggle("header__tab_active");
          } else {
            tab.classList.remove("header__tab_active");
          }
        });
      } else {
        headerWrapper.forEach((wrappers) => {
          wrappers.classList.remove("header__tab-wrapper_active");
        });
        headerTab.forEach((tabs) => {
          tabs.classList.remove("header__tab_active");
        });
      }
    });
  });

  document.addEventListener("click", (e) => {
    if (!e.target.closest(".header")) {
      headerWrapper.forEach((wrappers) => {
        wrappers.classList.remove("header__tab-wrapper_active");
      });
      headerTab.forEach((tabs) => {
        tabs.classList.remove("header__tab_active");
      });
    }
  });
}

document.addEventListener("scroll", () => {
  if (window.scrollY > 70) {
    header.classList.add("header_scroll");
  } else {
    header.classList.remove("header_scroll");
  }
});

// Кастомный курсор
let headerTabColumn = document.querySelectorAll(".header__tab-column");
let headerTabColumnIsHovered = false;
const customСursor = document.querySelector(".custom-cursor");

headerTabColumn.forEach((column) => {
  column.addEventListener("mouseenter", () => {
    headerTabColumnIsHovered = true;
  });
  column.addEventListener("mouseleave", () => {
    headerTabColumnIsHovered = false;
  });
});

const mouseMove = function (e) {
  let x = e.clientX;
  let y = e.clientY;
  customСursor.style.left = x + "px";
  customСursor.style.top = y + "px";
  if (headerTabBg.classList.contains("header__tab-bg_hovered") && !headerTabColumnIsHovered) {
    customСursor.classList.add("custom-cursor_active");
  } else {
    customСursor.classList.remove("custom-cursor_active");
  }
};

document.addEventListener("click", (e) => {
  if (e.target.classList.contains("header__tab-wrapper") || e.target.classList.contains("header__tab-list")) {
    customСursor.classList.remove("custom-cursor_active");
  }
});

document.addEventListener("mousemove", mouseMove);
;
document.addEventListener("DOMContentLoaded", () => {
  let html = document.querySelector("html");
  html.classList.add("is-dom-ready");
});

document.addEventListener("DOMContentLoaded", () => {
  const scrollItems = document.querySelectorAll(".scroll-item");
  const cards = document.querySelectorAll(".card");

  if (scrollItems.length) {
    const scrollAnimation = () => {
      scrollItems.forEach((el) => {
        let elementPosY = document.documentElement.clientHeight - el.offsetHeight;
        if (
          el.getBoundingClientRect().top < elementPosY ||
          el.getBoundingClientRect().top <
            document.documentElement.clientHeight / 1.5
        ) {
          el.classList.add("is-active");
        }
      });
      cards.forEach((card) => {
        let elementPosY =
          document.documentElement.clientHeight - card.offsetHeight;
        if (
          card.getBoundingClientRect().top < elementPosY ||
          card.getBoundingClientRect().top <
            document.documentElement.clientHeight / 1.05
        ) {
          card.classList.add("is-active");
        }
      });
    };
  
    scrollAnimation();
    window.addEventListener("scroll", () => {
      scrollAnimation();
    });
  }
});

// let patter2Group = document.querySelectorAll("#patter-2 .group");
// let cards = document.querySelectorAll(".card");

// function animatePatter2() {
//   patter2Group.forEach((group, i) => {
//     gsap.from(group, 1.5, {
//       x: "70vw",
//       delay: (i / 10) * 2 + 0.3,
//       ease: Power4.easeOut,
//     });
//   });
// }

// cards.forEach((card) => {
//   card.addEventListener("mouseover", () => {
//     animatePatter2();
//   });
// });

// document.addEventListener("DOMContentLoaded", () => {
//   animatePatter2();
// });

// let animationGroups = document.querySelectorAll(".group animateTransform");
// let cards = document.querySelectorAll(".card");

// animationGroups.forEach((group) => {
//   document.addEventListener("DOMContentLoaded", () => {
//     group.beginElement();
//   });
// });

// cards.forEach((card) => {
//   card.addEventListener("mouseover", () => {
//     animationGroups.forEach((group) => {
//       group.beginElement();
//     });
//   });
// });

// Анимация паттерна при наведении на карточку
let card = document.querySelectorAll(".card");

if (card.length) {
  card.forEach((item) => {
    let animationActive = false;
    let patterWrappers = item.querySelectorAll(".card__pic-bg-img");
    let lastAnimatedPatter = "";
    patterWrappers.forEach((wrapper) => {
      if (
        window.getComputedStyle(wrapper).getPropertyValue("display") === "block"
      ) {
        let wrapperPatters = wrapper.querySelectorAll(".patter__group");
        lastAnimatedPatter = wrapperPatters[wrapperPatters.length - 1];
      }
    });
    item.addEventListener("mouseenter", () => {
      if (!animationActive) {
        item.classList.add("card_patter-start-animation");
        animationActive = true;
        lastAnimatedPatter.addEventListener("animationend", () => {
          item.classList.remove("card_patter-start-animation");
          animationActive = false;
        });
      } else {
        lastAnimatedPatter.addEventListener("animationend", () => {
          item.classList.add("card_patter-start-animation");
          animationActive = true;
          lastAnimatedPatter.addEventListener("animationend", () => {
            item.classList.remove("card_patter-start-animation");
            animationActive = false;
          });
        });
      }
    });
    item.addEventListener("mouseleave", () => {
      if (!animationActive) {
        item.classList.add("card_patter-end-animation");
        animationActive = true;
        lastAnimatedPatter.addEventListener("animationend", () => {
          item.classList.remove("card_patter-end-animation");
          animationActive = false;
        });
      } else {
        lastAnimatedPatter.addEventListener("animationend", () => {
          item.classList.add("card_patter-end-animation");
          animationActive = true;
          lastAnimatedPatter.addEventListener("animationend", () => {
            item.classList.remove("card_patter-end-animation");
            animationActive = false;
          });
        });
      }
    });
  });
}

// Анимация карточек направлений при ховере
let productsCard = document.querySelectorAll(".products__card");

if (productsCard.length) {
  productsCard.forEach((card) => {
    card.addEventListener("mouseenter", () => {
      card.classList.remove("products__card_end-animation");
      card.classList.add("products__card_start-animation");
    });
    card.addEventListener("mouseleave", () => {
      card.classList.remove("products__card_start-animation");
      card.classList.add("products__card_end-animation");
    });
  });
}

;
// Функции создания разметки и рендера нативного селекта

const createElement = (template) => {
  const newElement = document.createElement('div');

  newElement.innerHTML = template;

  return newElement.firstChild;
};

const renderElement = (container, component, place = 'beforeend') => {
  switch (place) {
    case 'prepend':
      container.prepend(component);
      break;
    case 'afterend':
      container.after(component);
      break;
    case 'beforeend':
      container.append(component);
      break;
  }
};

const createNativeOptionsMarkup = (items, activeIndex) => {
  return items.map((el, index) => {
    if (activeIndex.length) {
      const currentIndex = activeIndex.find((item) => item === index);
      if (currentIndex === index) {
        return `<option ${el.value ? `value=${el.value}` : ''} selected>${el.text ? `${el.text}` : ''}</option>`;
      } else {
        return `<option ${el.value ? `value=${el.value}` : ''}>${el.text ? `${el.text}` : ''}</option>`;
      }
    } else {
      return `<option ${el.value ? `value=${el.value}` : ''}>${el.text ? `${el.text}` : ''}</option>`;
    }
  }).join('\n');
};

const createNativeSelectMarkup = ({id, items, multiple, name, required, activeIndex = []}) => {
  return `<select ${id ? `id='${id}'` : ''} ${name ? `name='${name}'` : ''} ${multiple ? 'multiple' : ''} ${
    required ? 'required' : ''
  } tabindex="-1" aria-hidden="true">
            <option value=""></option>
            ${createNativeOptionsMarkup(items, activeIndex)}
          </select>`;
};

// Функция расстановки активных состояний у li по умолчанию

const setActiveState = (multiple, selectItems) => {
  let flag = true;
  let activeIndex = [];
  selectItems.forEach((item, index) => {
    if (multiple) {
      if (item.getAttribute('aria-selected') === 'true') {
        activeIndex.push(index);
      }
    } else {
      if (item.getAttribute('aria-selected') === 'true' && flag) {
        activeIndex.push(index);
        flag = false;
      } else {
        item.setAttribute('aria-selected', 'false');
      }
    }
  });
  return activeIndex;
};

// Формирование строки для мультиселекта

const createMultiString = (arr) => {
  let str = '';
  if (arr.length) {
    if (arr.length === 1) {
      str = arr[0].innerHTML;
    } else {
      arr.forEach((el, index) => {
        if (index === arr.length - 1) {
          str += el.innerHTML;
        } else {
          str += `${el.innerHTML}, `;
        }
      });
    }
  }
  return str;
};

// Функция расстановки активных состояний у li по умолчанию

const setSelectActiveState = (multiple, insert, item) => {
  const buttonTextBlock = item.querySelector('.custom-select__text');
  const activeItems = item.querySelectorAll('.custom-select__item[aria-selected="true"]');
  const label = item.querySelector('.custom-select__label');
  const str = createMultiString(activeItems);

  buttonTextBlock.style.transition = '0s';
  label.style.transition = '0s';

  setTimeout(() => {
    label.style.transition = null;
    buttonTextBlock.style.transition = null;
  }, 300);

  if (multiple && insert) {
    item.classList.add('not-empty');
    buttonTextBlock.innerHTML = str;
  } else if (multiple) {
    return;
  } else {
    item.classList.add('not-empty');
    buttonTextBlock.innerHTML = activeItems[0].innerHTML;
  }
};

// Функция создания структуры селекта, после создания селекта она сразу его рендерит

const createSelectStructure = (item) => {
  const options = {};
  options.items = [];
  const multiple = item.dataset.multiple;
  const id = item.dataset.id;
  const name = item.dataset.name;
  const required = item.dataset.required;
  const insert = item.dataset.insert;
  const selectItems = item.querySelectorAll('.custom-select__item');
  const activeIndex = setActiveState(multiple, selectItems);

  if (activeIndex.length) {
    options.activeIndex = activeIndex;
    setSelectActiveState(multiple, insert, item);
  }

  options.name = name || false;
  options.id = id || false;
  options.required = Boolean(required);
  options.multiple = Boolean(multiple);

  selectItems.forEach((selectItem) => {
    const value = selectItem.dataset.selectValue;
    const itemInfo = {};
    itemInfo.text = selectItem.innerText;
    itemInfo.value = value;
    options.items.push(itemInfo);
  });

  renderElement(item, createElement(createNativeSelectMarkup(options)));
  return item;
};

// Закрытие селекта

const closeSelect = () => {
  const activeSelect = document.querySelector('[data-select].is-open');
  document.removeEventListener('click', onDocumentClick);
  document.removeEventListener('keydown', onEscapePress);
  if (activeSelect) {
    activeSelect.classList.remove('is-open');
  }
};

// Действия при клике на элемент списка

const clickAction = (el, index) => {
  const parent = el.closest('.custom-select');
  const multiple = parent.dataset.multiple;
  const insert = parent.dataset.insert;
  const buttonTextBlock = parent.querySelector('.custom-select__text');
  const itemText = el.innerText;
  const options = parent.querySelectorAll('option');
  const select = parent.querySelector('select');
  const changeEv = new CustomEvent('change');
  const inputEv = new CustomEvent('input');
  select.dispatchEvent(changeEv);
  select.dispatchEvent(inputEv);
  const form = select.closest('form');

  if (multiple) {
    if (insert === 'true') {
      if (el.getAttribute('aria-selected') === 'true') {
        el.setAttribute('aria-selected', 'false');
        const activeItems = parent.querySelectorAll('.custom-select__item[aria-selected="true"]');
        const str = createMultiString(activeItems);
        options[index + 1].selected = false;
        buttonTextBlock.innerText = str;
        if (!str) {
          parent.classList.remove('not-empty');
          parent.classList.remove('is-valid');
        }
      } else {
        el.setAttribute('aria-selected', 'true');
        const activeItems = parent.querySelectorAll('.custom-select__item[aria-selected="true"]');
        const str = createMultiString(activeItems);
        buttonTextBlock.innerText = str;
        parent.classList.add('not-empty');
        parent.classList.add('is-valid');
        options[index + 1].selected = true;
      }
    } else {
      if (el.getAttribute('aria-selected') === 'true') {
        el.setAttribute('aria-selected', 'false');
        options[index + 1].selected = false;
      } else {
        el.setAttribute('aria-selected', 'true');
        options[index + 1].selected = true;
      }
    }
  } else {
    const activeItem = parent.querySelector('.custom-select__item[aria-selected="true"]');
    if (el.getAttribute('aria-selected') === 'true') {
      closeSelect();
    } else {
      if (activeItem) {
        activeItem.setAttribute('aria-selected', 'false');
        parent.classList.remove('not-empty');
        parent.classList.remove('is-valid');
      }
      buttonTextBlock.innerText = itemText;
      el.setAttribute('aria-selected', 'true');
      parent.classList.add('not-empty');
      parent.classList.add('is-valid');
      options[index + 1].selected = true;
      closeSelect();
    }
  }

  if (form) {
    const formChangeEv = new CustomEvent('change');
    const formInputEv = new CustomEvent('input');
    form.dispatchEvent(formChangeEv);
    form.dispatchEvent(formInputEv);
  }

};

// Обработчики событий

const onDocumentClick = ({target}) => {
  if (!target.closest('.custom-select')) {
    closeSelect();
  }
};

const onEscapePress = (e) => {
  const isEscape = e.key === 'Escape';
  if (isEscape) {
    closeSelect();
  }
};

const onItemClick = (el, index) => {
  clickAction(el, index);
};

const onItemKeydown = (e, el, index) => {
  const isEnter = e.key === 'Enter';
  if (isEnter) {
    clickAction(el, index);
  }
};

const onLastItemKeydown = (e) => {
  const isTab = e.key === 'Tab';
  if (isTab) {
    closeSelect();
  }
};

const onSelectClick = (e) => {
  const parent = e.target.closest('[data-select]');
  const activeSelect = document.querySelector('[data-select].is-open');

  parent.classList.remove('is-invalid');

  if (activeSelect && activeSelect === parent) {
    activeSelect.classList.remove('is-open');
    return;
  }

  if (activeSelect) {
    closeSelect();
  }

  document.addEventListener('click', onDocumentClick);
  document.addEventListener('keydown', onEscapePress);

  if (parent.classList.contains('is-open')) {
    parent.classList.remove('is-open');
  } else {
    parent.classList.add('is-open');
  }
};

const onSelectKeydown = (e) => {
  const parent = e.target.closest('[data-select]');
  parent.classList.remove('is-invalid');
  if (e.shiftKey && e.key === 'Tab' && parent.closest('.is-open')) {
    closeSelect();
  }
};

// Все действия с селектом

const setSelectAction = (item) => {
  const customSelect = item;
  const button = customSelect.querySelector('.custom-select__button');
  const selectItems = customSelect.querySelectorAll('.custom-select__item');

  button.addEventListener('click', onSelectClick);
  button.addEventListener('keydown', onSelectKeydown);

  selectItems.forEach((el, index) => {
    el.addEventListener('click', () => {
      onItemClick(el, index);
    });

    el.addEventListener('keydown', (e) => {
      onItemKeydown(e, el, index);
    });

    if (index === selectItems.length - 1) {
      el.addEventListener('keydown', onLastItemKeydown);
    }
  });
};

// Класс CustomSelect

class CustomSelect {
  constructor() {
    window.selectInit = this.init.bind(this);
  }

  setAction(item) {
    setSelectAction(item);
  }

  createSelect(item) {
    createSelectStructure(item);
    return item;
  }

  initSelects() {
    const selects = document.querySelectorAll('[data-select]');
    if (selects.length) {
      selects.forEach((select) => {
        if (!select.classList.contains('is-initialized')) {
          const newSelect = this.createSelect(select);
          this.setAction(newSelect);
          select.classList.add('is-initialized');
        }
      });
    }
  }

  init() {
    this.initSelects();
  }
}

const select = new CustomSelect();

select.init();
;
(function (global, factory) {
  if (typeof define === "function" && define.amd) {
    define(['exports'], factory);
  } else if (typeof exports !== "undefined") {
    factory(exports);
  } else {
    var mod = {
      exports: {}
    };
    factory(mod.exports);
    global.bodyScrollLock = mod.exports;
  }
})(this, function (exports) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });

  function _toConsumableArray(arr) {
    if (Array.isArray(arr)) {
      for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) {
        arr2[i] = arr[i];
      }

      return arr2;
    } else {
      return Array.from(arr);
    }
  }

  // Older browsers don't support event options, feature detect it.

  // Adopted and modified solution from Bohdan Didukh (2017)
  // https://stackoverflow.com/questions/41594997/ios-10-safari-prevent-scrolling-behind-a-fixed-overlay-and-maintain-scroll-posi

  var hasPassiveEvents = false;
  if (typeof window !== 'undefined') {
    var passiveTestOptions = {
      get passive() {
        hasPassiveEvents = true;
        return undefined;
      }
    };
    window.addEventListener('testPassive', null, passiveTestOptions);
    window.removeEventListener('testPassive', null, passiveTestOptions);
  }

  var isIosDevice = typeof window !== 'undefined' && window.navigator && window.navigator.platform && /iP(ad|hone|od)/.test(window.navigator.platform);


  var locks = [];
  var documentListenerAdded = false;
  var initialClientY = -1;
  var previousBodyOverflowSetting = void 0;
  var previousBodyPaddingRight = void 0;

  // returns true if `el` should be allowed to receive touchmove events.
  var allowTouchMove = function allowTouchMove(el) {
    return locks.some(function (lock) {
      if (lock.options.allowTouchMove && lock.options.allowTouchMove(el)) {
        return true;
      }

      return false;
    });
  };

  var preventDefault = function preventDefault(rawEvent) {
    var e = rawEvent || window.event;

    // For the case whereby consumers adds a touchmove event listener to document.
    // Recall that we do document.addEventListener('touchmove', preventDefault, { passive: false })
    // in disableBodyScroll - so if we provide this opportunity to allowTouchMove, then
    // the touchmove event on document will break.
    if (allowTouchMove(e.target)) {
      return true;
    }

    // Do not prevent if the event has more than one touch (usually meaning this is a multi touch gesture like pinch to zoom).
    if (e.touches.length > 1) return true;

    if (e.preventDefault) e.preventDefault();

    return false;
  };

  var setOverflowHidden = function setOverflowHidden(options) {
    // Setting overflow on body/documentElement synchronously in Desktop Safari slows down
    // the responsiveness for some reason. Setting within a setTimeout fixes this.
    setTimeout(function () {
      // If previousBodyPaddingRight is already set, don't set it again.
      if (previousBodyPaddingRight === undefined) {
        var _reserveScrollBarGap = !!options && options.reserveScrollBarGap === true;
        var scrollBarGap = window.innerWidth - document.documentElement.clientWidth;

        if (_reserveScrollBarGap && scrollBarGap > 0) {
          previousBodyPaddingRight = document.body.style.paddingRight;
          document.body.style.paddingRight = scrollBarGap + 'px';
        }
      }

      // If previousBodyOverflowSetting is already set, don't set it again.
      if (previousBodyOverflowSetting === undefined) {
        previousBodyOverflowSetting = document.body.style.overflow;
        document.body.style.overflow = 'hidden';
        document.querySelector('html').style.overflow = 'hidden';
      }
    });
  };

  var restoreOverflowSetting = function restoreOverflowSetting() {
    // Setting overflow on body/documentElement synchronously in Desktop Safari slows down
    // the responsiveness for some reason. Setting within a setTimeout fixes this.
    setTimeout(function () {
      if (previousBodyPaddingRight !== undefined) {
        document.body.style.paddingRight = previousBodyPaddingRight;

        // Restore previousBodyPaddingRight to undefined so setOverflowHidden knows it
        // can be set again.
        previousBodyPaddingRight = undefined;
      }

      if (previousBodyOverflowSetting !== undefined) {
        document.body.style.overflow = previousBodyOverflowSetting;
        document.querySelector('html').style.overflow = previousBodyOverflowSetting;

        // Restore previousBodyOverflowSetting to undefined
        // so setOverflowHidden knows it can be set again.
        previousBodyOverflowSetting = undefined;
      }
    });
  };

  // https://developer.mozilla.org/en-US/docs/Web/API/Element/scrollHeight#Problems_and_solutions
  var isTargetElementTotallyScrolled = function isTargetElementTotallyScrolled(targetElement) {
    return targetElement ? targetElement.scrollHeight - targetElement.scrollTop <= targetElement.clientHeight : false;
  };

  var handleScroll = function handleScroll(event, targetElement) {
    var clientY = event.targetTouches[0].clientY - initialClientY;

    if (allowTouchMove(event.target)) {
      return false;
    }

    if (targetElement && targetElement.scrollTop === 0 && clientY > 0) {
      // element is at the top of its scroll.
      return preventDefault(event);
    }

    if (isTargetElementTotallyScrolled(targetElement) && clientY < 0) {
      // element is at the top of its scroll.
      return preventDefault(event);
    }

    event.stopPropagation();
    return true;
  };

  var disableBodyScroll = exports.disableBodyScroll = function disableBodyScroll(targetElement, options) {
    if (isIosDevice) {
      // targetElement must be provided, and disableBodyScroll must not have been
      // called on this targetElement before.
      if (!targetElement) {
        // eslint-disable-next-line no-console
        console.error('disableBodyScroll unsuccessful - targetElement must be provided when calling disableBodyScroll on IOS devices.');
        return;
      }

      if (targetElement && !locks.some(function (lock) {
        return lock.targetElement === targetElement;
      })) {
        var lock = {
          targetElement: targetElement,
          options: options || {}
        };

        locks = [].concat(_toConsumableArray(locks), [lock]);

        targetElement.ontouchstart = function (event) {
          if (event.targetTouches.length === 1) {
            // detect single touch.
            initialClientY = event.targetTouches[0].clientY;
          }
        };
        targetElement.ontouchmove = function (event) {
          if (event.targetTouches.length === 1) {
            // detect single touch.
            handleScroll(event, targetElement);
          }
        };

        if (!documentListenerAdded) {
          document.addEventListener('touchmove', preventDefault, hasPassiveEvents ? { passive: false } : undefined);
          documentListenerAdded = true;
        }
      }
    } else {
      setOverflowHidden(options);
      var _lock = {
        targetElement: targetElement,
        options: options || {}
      };

      locks = [].concat(_toConsumableArray(locks), [_lock]);
    }
  };

  var clearAllBodyScrollLocks = exports.clearAllBodyScrollLocks = function clearAllBodyScrollLocks() {
    if (isIosDevice) {
      // Clear all locks ontouchstart/ontouchmove handlers, and the references.
      locks.forEach(function (lock) {
        lock.targetElement.ontouchstart = null;
        lock.targetElement.ontouchmove = null;
      });

      if (documentListenerAdded) {
        document.removeEventListener('touchmove', preventDefault, hasPassiveEvents ? { passive: false } : undefined);
        documentListenerAdded = false;
      }

      locks = [];

      // Reset initial clientY.
      initialClientY = -1;
    } else {
      restoreOverflowSetting();
      locks = [];
    }
  };

  var enableBodyScroll = exports.enableBodyScroll = function enableBodyScroll(targetElement) {
    if (isIosDevice) {
      if (!targetElement) {
        // eslint-disable-next-line no-console
        console.error('enableBodyScroll unsuccessful - targetElement must be provided when calling enableBodyScroll on IOS devices.');
        return;
      }

      targetElement.ontouchstart = null;
      targetElement.ontouchmove = null;

      locks = locks.filter(function (lock) {
        return lock.targetElement !== targetElement;
      });

      if (documentListenerAdded && locks.length === 0) {
        document.removeEventListener('touchmove', preventDefault, hasPassiveEvents ? { passive: false } : undefined);

        documentListenerAdded = false;
      }
    } else {
      locks = locks.filter(function (lock) {
        return lock.targetElement !== targetElement;
      });
      if (!locks.length) {
        restoreOverflowSetting();
      }
    }
  };

  window.disableBodyScroll = disableBodyScroll;
  window.enableBodyScroll = enableBodyScroll;
});

;
const openModal = (modal, callback, preventScrollLock) => {
  modal.classList.add('modal--active');

  if (callback) {
    callback();
  }

  if (!preventScrollLock) {
    window.disableBodyScroll(modal, {
      reserveScrollBarGap: true,
    });
  }
};

const closeModal = (modal, callback, preventScrollLock) => {
  modal.classList.remove('modal--active');

  if (callback) {
    callback();
  }

  if (!preventScrollLock) {
    setTimeout(() => {
      window.enableBodyScroll(modal);
    }, 300);
  }
};

const onEscPress = (evt, modal, callback) => {
  const isEscKey = evt.key === 'Escape' || evt.key === 'Esc';

  if (isEscKey && modal.classList.contains('modal--active')) {
    evt.preventDefault();
    closeModal(modal, callback);
  }
};

const setModalListeners = (modal, closeCallback, preventScrollLock) => {
  const overlay = modal.querySelector('.modal__overlay');
  const closeBtn = modal.querySelector('.modal__close-btn');
  const closeBtnAlt = modal.querySelector('.modal__close-btn-alt');

  closeBtn.addEventListener('click', () => {
    closeModal(modal, closeCallback, preventScrollLock);
  });

  if (closeBtnAlt) {
    closeBtnAlt.addEventListener('click', () => {
      closeModal(modal, closeCallback, preventScrollLock);
    });
  }

  overlay.addEventListener('click', () => {
    closeModal(modal, closeCallback, preventScrollLock);
  });

  document.addEventListener('keydown', (evt) => {
    onEscPress(evt, modal, closeCallback, preventScrollLock);
  });
};

const setupModal = (modal, closeCallback, modalBtn, openCallback, noPrevDefault, preventScrollLock) => {
  if (modalBtn) {
    modalBtn.addEventListener('click', (evt) => {
      if (!noPrevDefault) {
        evt.preventDefault();
      }
      openModal(modal, openCallback, preventScrollLock);
    });
  }

  setModalListeners(modal, closeCallback, preventScrollLock);
};

// настраиваем модалки тут, все колбеки импортим, а не создаем из этого модуля простыню
const initModal = (modalId, modal, btn) => {
  switch (modalId) {
    case 'example':
      setupModal(modal, false, btn, false, true, true);
      break;
    default:
      setupModal(modal, false, btn, false, false, false);
      break;
  }
};

const modalSubscribeSuccessMessage = document.querySelector('.modal--subscribe-success-message');

if (modalSubscribeSuccessMessage) {
  setupModal(modalSubscribeSuccessMessage);
}

const modalRegistrationSuccessMessage = document.querySelector('.modal--registration-success-message');

if (modalRegistrationSuccessMessage) {
  setupModal(modalRegistrationSuccessMessage);
}


// аргументы setupModal(modal, closeCallback, modalBtns, openCallback, noPrevDefault, preventScrollLock)
// возможна инициализация только с первыми аргументом,
// если вам нужно открывать модалку в другом месте под какими-нибудь условиями
const initModals = () => {
  const modals = document.querySelectorAll('.modal:not(.is-initialized)');
  const modalBtns = document.querySelectorAll('[data-modal]');

  // фикс для редких случаев, когда модалка появляется при загрузке страницы
  if (modals.length) {
    modals.forEach((el) => {
      setTimeout(() => {
        el.classList.remove('modal--preload');
        el.classList.add('is-initialized');
      }, 100);
    });
  }

  if (modalBtns.length) {
    modalBtns.forEach((btn) => {
      const modalId = btn.dataset.modal;
      const modal = document.querySelector(`.modal--${modalId}`);
      if (modal) {
        initModal(modalId, modal, btn);
      }
    });
  }
};

window.initModals = initModals();
;
// Константы и переменные

const BASE_COUNTRY_CODE = '+7';
const BASE_MATRIX = '(___) ___ __ __';
const phoneLength = BASE_COUNTRY_CODE.length + BASE_MATRIX.length;

// Ограничения ввода для обычных полей

const returnLimitationsRegEx = (dataLimitations) => {
  switch (dataLimitations) {
    case 'digit':
      return /[^\d]/g;
    case 'letters':
      return /[^a-zA-Zа-яёА-ЯЁ\s]/g;
    case 'letters-and-digit':
      return /[^a-zA-Zа-яёА-ЯЁ\s\d]/g;
    case 'cyrillic':
      return /[^а-яёА-ЯЁ\s]/g;
    case 'latin':
      return /[^a-zA-Z\s]/g;
    default:
      return false;
  }
};

const simpleLimitations = (formElement, dataLimitations) => {
  const RegEx = returnLimitationsRegEx(dataLimitations);
  if (RegEx) {
    formElement.addEventListener('input', () => {
      formElement.value = formElement.value.replace(RegEx, '');
    });
  } else {
    // eslint-disable-next-line no-console
    console.error(`Переданный формат ограничения: ${dataLimitations}, не поддерживается. Проверьте корректность введённых значений в соответствии со спецификацией.`);
  }
};

// Ограничения ввода для обычных полей с матрицей

const returnMatrixLimitationsRegEx = (dataMatrixLimitations) => {
  switch (dataMatrixLimitations) {
    case 'digit':
      return /[^\d]/g;
    case 'letters':
      return /[^\а-яё\А-ЯЁ\a-z\A-Z]/g;
    case 'letters-and-digit':
      return /[^\а-яё\А-ЯЁ\a-z\A-Z\d]/g;
    case 'cyrillic':
      return /[^\а-яё\А-ЯЁ]/g;
    case 'latin':
      return /[^\a-z\A-Z]/g;
    default:
      return false;
  }
};

const initMatrixReplace = (target, matrix, RegEx) => {
  const def = matrix.replace(RegEx, '');
  let val = target.value.replace(RegEx, '');
  let i = 0;

  if (def.length >= val.length) {
    val = def;
  }

  target.value = matrix.replace(/./g, (a) => {
    if (/[_\^]/.test(a) && i < val.length) {
      return val.charAt(i++);
    } else if (i >= val.length) {
      return '';
    } else {
      return a;
    }
  });
};

const simpleMatrix = (formElement, dataMatrix, dataMatrixLimitations) => {
  if (dataMatrixLimitations === null) {
    // eslint-disable-next-line no-console
    console.error('При валидации по матрице обязательно указывать формат ограничений: data-matrix-limitations=""');
    return;
  }
  const RegEx = returnMatrixLimitationsRegEx(dataMatrixLimitations);
  if (RegEx) {

    formElement.addEventListener('input', () => {
      initMatrixReplace(formElement, dataMatrix, RegEx);
    });
  } else {
    // eslint-disable-next-line no-console
    console.error(`Переданный формат ограничения: ${dataMatrixLimitations}, не поддерживается. Проверьте корректность введённых значений в соответствии со спецификацией.`);
  }
};

// Маска для телефона

const onInputPhoneInput = ({target}) => {
  const matrix = `${BASE_COUNTRY_CODE}${BASE_MATRIX}`;
  const def = matrix.replace(/\D/g, '');
  let i = 0;
  let val = target.value.replace(/\D/g, '');
  if (def.length >= val.length) {
    val = def;
  }
  target.value = matrix.replace(/./g, (a) => {
    if (/[_\d]/.test(a) && i < val.length) {
      return val.charAt(i++);
    } else if (i >= val.length) {
      return '';
    } else {
      return a;
    }
  });
};

const prettifyPhoneInput = (input) => {
  if (!input.value.startsWith(BASE_COUNTRY_CODE)) {
    if (input.value.startsWith('8')) {
      input.value = input.value.replace('8', BASE_COUNTRY_CODE);
    } else {
      input.value = `${BASE_COUNTRY_CODE}${input.value}`;
    }
  }
  // onInputPhoneInput({input});
  const matrix = `${BASE_COUNTRY_CODE}${BASE_MATRIX}`;
  const def = matrix.replace(/\D/g, '');
  let i = 0;
  let val = input.value.replace(/\D/g, '');
  if (def.length >= val.length) {
    val = def;
  }

  input.value = matrix.replace(/./g, (a) => {
    if (/[_\d]/.test(a) && i < val.length) {
      return val.charAt(i++);
    } else if (i >= val.length) {
      return '';
    } else {
      return a;
    }
  });
};

const onFocusPhoneInput = ({target}) => {
  if (!target.value) {
    target.value = BASE_COUNTRY_CODE;
  }

  target.addEventListener('input', onInputPhoneInput);
  target.addEventListener('blur', onBlurPhoneInput);
  target.addEventListener('keydown', onKeydownPhoneInput);
};

const onKeydownPhoneInput = (e) => {
  if (e.target.selectionStart === 1 && e.keyCode === 8 || e.keyCode === 46) {
    e.preventDefault();
  }
  if (e.target.selectionStart <= phoneLength && e.keyCode !== 8 && e.keyCode !== 46 && e.keyCode !== 37 && e.keyCode !== 39) {
    e.target.setSelectionRange(phoneLength, phoneLength);
  }
};

const onBlurPhoneInput = ({target}) => {
  if (target.value === BASE_COUNTRY_CODE) {
    const parent = target.closest('[data-validate-type="phone"]');
    target.value = '';
    parent.classList.remove('not-empty');
    target.removeEventListener('input', onInputPhoneInput);
    target.removeEventListener('blur', onBlurPhoneInput);
  }
};

// Показ ошибок полей форм

const hideError = (el) => {
  if (el.classList.contains('toggle-group')) {
    validateToggleGroup(el);
    el.setAttribute('aria-invalid', 'true');
    el.classList.remove('is-invalid');
    el.classList.add('is-valid');
  } else {
    const parent = el.closest('[data-validate-type]');
    el.setAttribute('aria-invalid', 'false');
    parent.classList.remove('is-invalid');
    parent.classList.add('is-valid');
  }
};

const showError = (el) => {
  if (el.classList.contains('toggle-group')) {
    validateToggleGroup(el);
    el.setAttribute('aria-invalid', 'false');
    el.classList.add('is-invalid');
    el.classList.remove('is-valid');
  } else {
    const parent = el.closest('[data-validate-type]');
    el.setAttribute('aria-invalid', 'true');
    parent.classList.add('is-invalid');
    parent.classList.remove('is-valid');
  }
};

const showInputsError = (inputs) => {
  let flag = true;
  let result = true;
  inputs.forEach((input) => {
    const type = input.closest('[data-validate-type]').dataset.validateType;
    if (type === 'toggle-group') {
      return;
    }
    flag = validateInputs(type, input);
    if (!flag) {
      result = flag;
      showError(input);
    } else {
      hideError(input);
    }
  });
  return result;
};

const showGroupInputsError = (parents) => {
  let flag = true;
  let result = true;
  if (!parents.length) {
    return result;
  }
  parents.forEach((parent) => {
    flag = validateToggleGroup(parent);
    if (!flag) {
      result = flag;
      showError(parent);
    } else {
      hideError(parent);
    }
  });
  return result;
};

const showErrors = (inputs, parents) => {
  let result = true;
  const inputsResult = showInputsError(inputs);
  const groupResult = showGroupInputsError(parents);

  if (!inputsResult || !groupResult) {
    result = false;
  }

  return result;
};

// Валидация полей форм

const validateTextInput = (input) => {
  const parent = input.closest('[data-validate-type]');
  parent.classList.remove('is-invalid');
  let flag = true;
  let minLength = +input.getAttribute('minlength');
  if (!minLength) {
    minLength = 1;
  }
  if (input.value.length >= minLength) {
    parent.classList.add('is-valid');
    input.setAttribute('aria-invalid', 'false');
  } else {
    parent.classList.remove('is-valid');
    input.setAttribute('aria-invalid', 'true');
    flag = false;
  }
  return flag;
};

const validatePhoneInput = (input) => {
  const parent = input.closest('[data-validate-type]');
  parent.classList.remove('is-invalid');
  let flag = true;
  if (input.value.length >= phoneLength) {
    parent.classList.add('is-valid');
    input.setAttribute('aria-invalid', 'false');
  } else {
    parent.classList.remove('is-valid');
    input.setAttribute('aria-invalid', 'true');
    flag = false;
  }
  return flag;
};

const validateEmailInput = (input) => {
  const parent = input.closest('[data-validate-type]');
  parent.classList.remove('is-invalid');
  let flag = true;
  const emailString = /[a-zA-Zа-яёА-ЯЁ0-9]{1}([a-zA-Zа-яёА-ЯЁ0-9\-_\.]{1,})?@[a-zA-Zа-яёА-ЯЁ0-9\-]{1}([a-zA-Zа-яёА-ЯЁ0-9.\-]{1,})?[a-zA-Zа-яёА-ЯЁ0-9\-]{1}\.[a-zA-Zа-яёА-ЯЁ]{2,6}/;
  const regEmail = new RegExp(emailString, '');
  if (regEmail.test(input.value)) {
    parent.classList.add('is-valid');
    input.setAttribute('aria-invalid', 'false');
  } else {
    parent.classList.remove('is-valid');
    input.setAttribute('aria-invalid', 'true');
    flag = false;
  }
  return flag;
};

const validateMatrixInput = (input) => {
  const parent = input.closest('[data-validate-type]');
  parent.classList.remove('is-invalid');
  let flag = true;
  const matrix = input.closest('[data-matrix]').dataset.matrix;
  if (input.value.length === matrix.length) {
    parent.classList.add('is-valid');
    input.setAttribute('aria-invalid', 'false');
  } else {
    parent.classList.remove('is-valid');
    input.setAttribute('aria-invalid', 'true');
    flag = false;
  }
  return flag;
};

const findSelectedOption = (options) => {
  let flag = false;
  options.forEach((option) => {
    if (option.value && option.selected) {
      flag = true;
    }
  });
  return flag;
};

const validateSelect = (input) => {
  const parent = input.closest('[data-validate-type]');
  const options = input.querySelectorAll('option');
  const customSelectText = parent.querySelector('.custom-select__text');
  parent.classList.remove('is-invalid');
  input.setAttribute('aria-invalid', 'false');
  let flag = true;
  if (findSelectedOption(options)) {
    parent.classList.add('is-valid');
    input.setAttribute('aria-invalid', 'false');
  } else {
    parent.classList.remove('is-valid');
    input.setAttribute('aria-invalid', 'true');
    customSelectText.innerHTML = '';
    parent.classList.remove('not-empty');
    flag = false;
  }
  return flag;
};

const validateCheckbox = (input) => {
  const parent = input.closest('[data-validate-type]');
  parent.classList.remove('is-invalid');
  let flag = true;
  if (input.checked) {
    parent.classList.add('is-valid');
  } else {
    parent.classList.remove('is-valid');
    flag = false;
  }
  return flag;
};

const validateInputs = (type, input) => {
  switch (type) {
    case 'text':
      return validateTextInput(input);
    case 'phone':
      return validatePhoneInput(input);
    case 'email':
      return validateEmailInput(input);
    case 'matrix':
      return validateMatrixInput(input);
    case 'select':
      return validateSelect(input);
    case 'checkbox':
      return validateCheckbox(input);
    default:
      return false;
  }
};

const returnCheckedElements = (inputs) => {
  let flag = false;
  inputs.forEach((input) => {
    if (input.checked) {
      flag = true;
    }
  });
  return flag;
};

const removeGroupAria = (inputs) => {
  inputs.forEach((input) => {
    if (!input.checked) {
      input.removeAttribute('aria-required');
      input.removeAttribute('aria-invalid');
    } else {
      input.setAttribute('aria-required', true);
      input.setAttribute('aria-invalid', false);
    }
  });
};

const setGroupAria = (inputs) => {
  inputs.forEach((input) => {
    input.setAttribute('aria-required', true);
    input.setAttribute('aria-invalid', true);
  });
};

const validateToggleGroup = (parent) => {
  const formElements = parent.querySelectorAll('input');
  parent.classList.remove('is-invalid');
  let flag = true;
  if (returnCheckedElements(formElements)) {
    removeGroupAria(formElements);
    parent.classList.add('is-valid');
  } else {
    setGroupAria(formElements);
    parent.classList.remove('is-valid');
    flag = false;
  }
  return flag;
};

const checkInputValidity = (type, input) => {
  validateInputs(type, input);
};

// Установка всех действий в полях форм

const formElementLimitationsAction = (formValidateElement) => {
  const dataLimitations = formValidateElement.dataset.limitations ? formValidateElement.dataset.limitations : null;
  let formElement = formValidateElement.querySelector('input');
  if (!formElement) {
    formElement = formValidateElement.querySelector('textarea');
  }
  if (!formElement) {
    // eslint-disable-next-line no-console
    console.error('В валидируемом блоке отсутствует поле формы');
    return;
  }
  if (dataLimitations) {
    simpleLimitations(formElement, dataLimitations);
  }
};

const formElementMatrixAction = (formValidateElement) => {
  const dataMatrix = formValidateElement.dataset.matrix ? formValidateElement.dataset.matrix : null;
  const dataMatrixLimitations = formValidateElement.dataset.matrixLimitations ? formValidateElement.dataset.matrixLimitations : null;
  let formElement = formValidateElement.querySelector('input');
  if (!formElement) {
    formElement = formValidateElement.querySelector('textarea');
  }
  if (!formElement) {
    // eslint-disable-next-line no-console
    console.error('В валидируемом блоке отсутствует поле формы');
    return;
  }
  if (dataMatrix) {
    simpleMatrix(formElement, dataMatrix, dataMatrixLimitations);
  }
};

const formElementValidateAction = (formValidateElement) => {
  const dataValidateType = formValidateElement.dataset.validateType;
  const dataLimitations = formValidateElement.dataset.limitations ? formValidateElement.dataset.limitations : null;
  const dataMatrix = formValidateElement.dataset.matrix ? formValidateElement.dataset.matrix : null;
  const dataMatrixLimitations = formValidateElement.dataset.matrixLimitations ? formValidateElement.dataset.matrixLimitations : null;
  if (dataValidateType !== 'toggle-group') {
    let formElement = formValidateElement.querySelector('input');
    if (!formElement) {
      formElement = formValidateElement.querySelector('textarea');
    }
    if (!formElement) {
      formElement = formValidateElement.querySelector('select');
    }
    if (!formElement) {
      // eslint-disable-next-line no-console
      console.error('В валидируемом блоке отсутствует поле формы');
      return;
    }

    formElement.setAttribute('aria-required', true);
    formElement.setAttribute('aria-invalid', true);

    if (dataLimitations) {
      simpleLimitations(formElement, dataLimitations);
    }

    if (dataMatrix) {
      simpleMatrix(formElement, dataMatrix, dataMatrixLimitations);
    }

    if (dataValidateType === 'phone') {
      if (formElement.value) {
        prettifyPhoneInput(formElement);
      }
      formElement.addEventListener('focus', onFocusPhoneInput);
    }

    formElement.addEventListener('input', () => {
      checkInputValidity(dataValidateType, formElement);
    });

    checkInputValidity(dataValidateType, formElement);
  } else {
    const formElements = formValidateElement.querySelectorAll('input');
    if (formElements.length) {
      formElements.forEach((el) => {
        el.setAttribute('aria-required', true);
        el.setAttribute('aria-invalid', true);
        el.addEventListener('input', () => {
          validateToggleGroup(formValidateElement);
        });
      });
      validateToggleGroup(formValidateElement);
    } else {
      // eslint-disable-next-line no-console
      console.error('В валидируемом блоке отсутствуют поля формы');
      return;
    }
  }
};

// Обработка события submit на форме

const onFormSubmit = (e, callback) => {
  const formElements = e.target.querySelectorAll('[aria-required="true"]');
  const groupsFormElement = e.target.querySelectorAll('[data-validate-type="toggle-group"]');
  if (showErrors(formElements, groupsFormElement) && callback && callback.successCallback) {
    callback.successCallback(e);
  } else if (callback && callback.errorCallback) {
    callback.errorCallback(e);
  } else {
    e.preventDefault();
  }
};

// Очистка полей формы

const clearForm = (form) => {
  form.reset();
  const formValidateElements = form.querySelectorAll('[data-validate-type]');
  const notEmptyInputs = form.querySelectorAll('.not-empty');
  if (notEmptyInputs.length) {
    notEmptyInputs.forEach((notEmptyInput) => {
      notEmptyInput.classList.remove('not-empty');
    });
  }
  formValidateElements.forEach((formValidateElement) => {
    const dataValidateType = formValidateElement.dataset.validateType;
    if (dataValidateType !== 'toggle-group') {
      let formElement = formValidateElement.querySelector('input');
      if (!formElement) {
        formElement = formValidateElement.querySelector('textarea');
      }
      if (!formElement) {
        formElement = formValidateElement.querySelector('select');
      }
      if (!formElement) {
        // eslint-disable-next-line no-console
        console.error('В валидируемом блоке отсутствует поле формы');
        return;
      }
      formElement.value = '';
      checkInputValidity(dataValidateType, formElement);
    } else {
      validateToggleGroup(formValidateElement);
    }
  });
};

window.clearForm = clearForm;

// Класс FormsValidate

class FormsValidate {
  constructor(wrappers, callback = {}) {
    this.wrappers = wrappers;
    this.callback = callback;
  }

  init(formWrappers) {
    if (!formWrappers) {
      formWrappers = this.wrappers;
    }

    const typeOfNode = Object.prototype.toString.call(formWrappers);

    if (
      typeOfNode === '[object HTMLCollection]' ||
      typeOfNode === '[object NodeList]' ||
      typeOfNode === '[object Array]'
    ) {
      formWrappers.forEach((wrapper) => {
        if (wrapper.classList.contains('is-initialized')) {
          // eslint-disable-next-line no-console
          console.error('На данной форме валидация уже инициализированна');
        } else {
          this.initItem(wrapper);
        }
      });
    } else if (typeOfNode === '[object HTMLDivElement]' || typeOfNode === '[object HTMLElement]') {
      if (!formWrappers.classList.contains('is-initialized')) {
        this.initItem(formWrappers);
      }
    } else {
      // eslint-disable-next-line no-console
      console.error('Переданный обьект не соответствует формату');
      return;
    }
  }

  initItem(element) {
    element.classList.add('is-initialized');
    const form = element.querySelector('form');
    const resetButtons = form.querySelectorAll('button[type="reset"], [data-reset]');
    form.noValidate = true;
    form.addEventListener('submit', (e) => {
      onFormSubmit(e, this.callback);
    });
    if (resetButtons.length) {
      resetButtons.forEach((btn) => {
        btn.addEventListener('click', (e) => {
          e.preventDefault();
          clearForm(form);
        });
      });
    }
    const formValidateElements = form.querySelectorAll('[data-validate-type]');
    const formLimitationsElements = form.querySelectorAll('[data-limitations]:not([data-validate-type])');
    const formMatrixElements = form.querySelectorAll('[data-matrix]:not([data-validate-type])');

    if (formValidateElements.length) {
      formValidateElements.forEach((el) => {
        formElementValidateAction(el);
      });
    }
    if (formLimitationsElements.length) {
      formLimitationsElements.forEach((el) => {
        formElementLimitationsAction(el);
      });
    }
    if (formMatrixElements.length) {
      formMatrixElements.forEach((el) => {
        formElementMatrixAction(el);
      });
    }
  }
}

window.FormsValidate = FormsValidate;
;
const formWrappers = [...document.querySelectorAll('[data-validate]')];

const resetForm = (form) => {
  setTimeout(() => {
    window.clearForm(form);
  }, 1000);
};

const baseSuccessCallback = (e) => {
  e.preventDefault();
  resetForm(e.target);
};

const baseErrorCallback = (e) => {
  e.preventDefault();
};

const subscribeFormSuccessCallback = (e) => {
  e.preventDefault();
  resetForm(e.target);

  closeModal(e.target.closest('.modal'));

  openModal(modalSubscribeSuccessMessage);
};

const eventRegistrationSuccessCallback = (e) => {
  e.preventDefault();
  resetForm(e.target);

  closeModal(e.target.closest('.modal'));

  openModal(modalRegistrationSuccessMessage);
};

const customExampleSuccessCallback = (e) => {
  e.preventDefault();
  resetForm(e.target);
  // eslint-disable-next-line no-console
  console.log('Ваша форма успешна отправлена');
};

const customExampleErrorCallback = (e) => {
  e.preventDefault();
  // eslint-disable-next-line no-console
  console.error('Отправка формы невозможна, заполните все обязательные поля');
};

const callbacks = {
  base: {
    successCallback: baseSuccessCallback,
    errorCallback: baseErrorCallback,
  },
  subscribeForm: {
    successCallback: subscribeFormSuccessCallback,
    errorCallback: baseErrorCallback,
  },
  eventRegistration: {
    successCallback: eventRegistrationSuccessCallback,
    errorCallback: baseErrorCallback,
  },
  customExample: {
    successCallback: customExampleSuccessCallback,
    errorCallback: customExampleErrorCallback,
  },
};

const initFormValidate = () => {
  if (formWrappers.length) {
    formWrappers.forEach((wrapper) => {
      let callback = wrapper.dataset.callback;
      if (!callback) {
        callback = 'base';
      }

      const formValidate = new window.FormsValidate(wrapper, callbacks[callback]);

      return formValidate.init();
    });
  }
};

initFormValidate();
;
let someChapter = document.querySelector("#some-chapter");
let projectsPageChapterTitle = document.querySelectorAll(".projects-page__chapter-title");
let customSelectText = document.querySelector(".custom-select__text");

if (someChapter) {
  someChapter.addEventListener("change", () => {
    setTimeout(() => {
      let url = `${window.location.origin}${window.location.pathname}#chapter-${someChapter.value}`;
      window.location.href = url;
    }, 100);
  });

  document.addEventListener("scroll", () => {
    let maxTop = 0;
    let currentTitleIndex;

    projectsPageChapterTitle.forEach((title, i) => {
      if (title.getBoundingClientRect().top < 150 && title.getBoundingClientRect().top > maxTop) {
        maxTop = title.getBoundingClientRect().top;
        currentTitleIndex = i;
      }
    });

    if (currentTitleIndex > -1) {
      let textTitle = projectsPageChapterTitle[currentTitleIndex].querySelector(".animate-item").textContent;
      if (customSelectText.textContent !== textTitle) {
        customSelectText.textContent = textTitle;
      }
    }
  });
}

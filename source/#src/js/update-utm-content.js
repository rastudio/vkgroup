function updateUtmContent() {
  function getAllUrlParams() {
    // извлекаем строку из объекта window
    var queryString = window.location.search.slice(1);

    // объект для хранения параметров
    var obj = {};

    // если есть строка запроса
    if (queryString) {
      // данные после знака # будут опущены
      queryString = queryString.split("#")[0];

      // разделяем параметры
      var arr = queryString.split("&");

      for (var i = 0; i < arr.length; i++) {
        // разделяем параметр на ключ => значение
        var a = arr[i].split("=");

        // обработка данных вида: list[]=thing1&list[]=thing2
        var paramNum = undefined;
        var paramName = a[0].replace(/\[\d*\]/, function (v) {
          paramNum = v.slice(1, -1);
          return "";
        });

        // передача значения параметра ('true' если значение не задано)
        var paramValue = typeof a[1] === "undefined" ? "" : a[1];

        // преобразование регистра
        paramName = paramName.toLowerCase();
        paramValue = paramValue.toLowerCase();

        // если ключ параметра уже задан
        if (obj[paramName]) {
          // преобразуем текущее значение в массив
          if (typeof obj[paramName] === "string") {
            obj[paramName] = [obj[paramName]];
          }
          // если не задан индекс...
          if (typeof paramNum === "undefined") {
            // помещаем значение в конец массива
            obj[paramName].push(paramValue);
          }
          // если индекс задан...
          else {
            // размещаем элемент по заданному индексу
            obj[paramName][paramNum] = paramValue;
          }
        }
        // если параметр не задан, делаем это вручную
        else {
          obj[paramName] = paramValue;
        }
      }
    }

    return obj;
  }

  function updateElement(selector, attribute, value) {
    let elements = document.querySelectorAll(selector);

    elements.forEach((element) => {
      element[attribute] = value;
    });
  }

  let resultParams = getAllUrlParams();

  let mtSub = resultParams['utm_campaign'] ? `&mt_sub4=${resultParams['utm_campaign']}` : "";

  let whiteList = {
    "utm_source": [
      {
        selector: ".landing-page-offer__btn",
        attribute: "href",
        values: {
          "": `https://trk.mail.ru/c/vrhqn5?mt_campaign=store_promo_75rub&mt_creative=2021oct&mt_network=rebranding${mtSub}`,
          web: `https://trk.mail.ru/c/vrhqn5?mt_campaign=store_promo_75rub&mt_creative=2021oct&mt_network=rebranding${mtSub}`,
          mobile: `https://trk.mail.ru/c/vrhqn5?mt_campaign=store_promo_75rub&mt_creative=2021oct&mt_network=rebranding${mtSub}`,
        },
      },
    ],
    "utm_campaign": [
      {
        selector: ".landing-page-promo__title",
        attribute: "textContent",
        values: {
          "": "VK становится больше",
          vk_landing: "VK становится больше",
          ok_landing: "Теперь Одноклассники — часть экосистемы VK",
          mail_landing: "Теперь Mail.ru — часть экосистемы VK",
          marusia_landing: "Теперь Маруся — часть экосистемы VK",
          boom_landing: "Теперь Boom — часть экосистемы VK",
          capsula_landing: "Теперь Капсула — часть экосистемы VK",
          atom_landing: "Теперь Атом — часть экосистемы VK",
          apteka_landing: "Теперь Все Аптеки — часть экосистемы VK",
          youla_landing: "Теперь Юла — часть экосистемы VK",
          vkjob_landing: "Теперь VK Работа — часть экосистемы VK",
          vkclassifieds_landing: "Теперь VK Объявления — часть экосистемы VK",
          mygames_landing: "Теперь My.Games — часть экосистемы VK",
          delivery_landing: "Теперь Delivery Club — часть экосистемы VK",
          citymobil_landing: "Теперь Ситимобил— часть экосистемы VK",
          citydrive_landing: "Теперь Ситидрайв — часть экосистемы VK",
          samokat_landing: "Теперь Самокат — часть экосистемы VK",
          skillbox_landing: "Теперь Skillbox— часть экосистемы VK",
          uchi_landing: "Теперь Учи.ру — часть экосистемы VK",
          geekbrains_landing: "Теперь Geekbrains — часть экосистемы VK",
          algoritmika_landing: "Теперь Алгоритмика — часть экосистемы VK",
          tetrika_landing: "Теперь Тетрика — часть экосистемы VK",
          skillfactory_landing: "Теперь Skill factory — часть экосистемы VK",
          contented_landing: "Теперь Contented — часть экосистемы VK",
          productlive_landing: "Теперь Product Live — часть экосистемы VK",
          reco_landing: "VK становится больше",
          remains_vk_landing: "VK становится больше",
          remains_ok_landing: "Теперь Одноклассники — часть экосистемы VK",
        }
      },
      {
        selector: ".landing-promo__bg-pic_desktop",
        attribute: "src",
        values: {
          "": "../../img/landing-promo-3.jpg",
          vk_landing: "../../img/landing-promo-3.jpg",
          ok_landing: "../../img/landing-promo-5.jpg",
          mail_landing: "../../img/landing-promo-1.jpg",
          boom_landing: "../../img/landing-promo-18.jpg",
          marusia_landing: "../../img/landing-promo-19.jpg",
          capsula_landing: "../../img/landing-promo-19.jpg",
          atom_landing: "../../img/landing-promo-20.jpg",
          apteka_landing: "../../img/landing-promo-2.jpg",
          youla_landing: "../../img/landing-promo-4.jpg",
          vkjob_landing: "../../img/landing-promo-6.jpg",
          vkclassifieds_landing: "../../img/landing-promo-3.jpg",
          mygames_landing: "../../img/landing-promo-7.jpg",
          delivery_landing: "../../img/landing-promo-8.jpg",
          citymobil_landing: "../../img/landing-promo-9.jpg",
          citydrive_landing: "../../img/landing-promo-10.jpg",
          samokat_landing: "../../img/landing-promo-11.jpg",
          skillbox_landing: "../../img/landing-promo-12.jpg",
          uchi_landing: "../../img/landing-promo-14.jpg",
          geekbrains_landing: "../../img/landing-promo-15.jpg",
          algoritmika_landing: "../../img/landing-promo-16.jpg",
          tetrika_landing: "../../img/landing-promo-17.jpg",
          skillfactory_landing: "../../img/landing-promo-13.jpg",
          contented_landing: "../../img/landing-promo-3.jpg",
          productlive_landing: "../../img/landing-promo-3.jpg",
          reco_landing: "../../img/landing-promo-3.jpg",
          remains_vk_landing: "../../img/landing-promo-3.jpg",
          remains_ok_landing: "../../img/landing-promo-3.jpg",
        }
      },
      {
        selector: ".landing-promo__bg-pic_desktop",
        attribute: "srcset",
        values: {
          "": "../../img/landing-promo-3@2x.jpg 2x",
          vk_landing: "../../img/landing-promo-3@2x.jpg 2x",
          ok_landing: "../../img/landing-promo-5@2x.jpg 2x",
          mail_landing: "../../img/landing-promo-1@2x.jpg 2x",
          boom_landing: "../../img/landing-promo-18@2x.jpg 2x",
          marusia_landing: "../../img/landing-promo-19@2x.jpg 2x",
          capsula_landing: "../../img/landing-promo-19@2x.jpg 2x",
          atom_landing: "../../img/landing-promo-20@2x.jpg 2x",
          apteka_landing: "../../img/landing-promo-2@2x.jpg 2x",
          youla_landing: "../../img/landing-promo-4@2x.jpg 2x",
          vkjob_landing: "../../img/landing-promo-6@2x.jpg 2x",
          vkclassifieds_landing: "../../img/landing-promo-3@2x.jpg 2x",
          mygames_landing: "../../img/landing-promo-7@2x.jpg 2x",
          delivery_landing: "../../img/landing-promo-8@2x.jpg 2x",
          citymobil_landing: "../../img/landing-promo-9@2x.jpg 2x",
          citydrive_landing: "../../img/landing-promo-10@2x.jpg 2x",
          samokat_landing: "../../img/landing-promo-11@2x.jpg 2x",
          skillbox_landing: "../../img/landing-promo-12@2x.jpg 2x",
          uchi_landing: "../../img/landing-promo-14@2x.jpg 2x",
          geekbrains_landing: "../../img/landing-promo-15@2x.jpg 2x",
          algoritmika_landing: "../../img/landing-promo-16@2x.jpg 2x",
          tetrika_landing: "../../img/landing-promo-17@2x.jpg 2x",
          skillfactory_landing: "../../img/landing-promo-13@2x.jpg 2x",
          contented_landing: "../../img/landing-promo-3@2x.jpg 2x",
          productlive_landing: "../../img/landing-promo-3@2x.jpg 2x",
          reco_landing: "../../img/landing-promo-3@2x.jpg 2x",
          remains_vk_landing: "../../img/landing-promo-3@2x.jpg 2x",
          remains_ok_landing: "../../img/landing-promo-3@2x.jpg 2x",
        }
      },
      {
        selector: ".landing-promo__bg-pic_mobile",
        attribute: "src",
        values: {
          "": "../../img/landing-promo-1-mobile.jpg",
          vk_landing: "../../img/landing-promo-3-mobile.jpg",
          ok_landing: "../../img/landing-promo-5-mobile.jpg",
          mail_landing: "../../img/landing-promo-1-mobile.jpg",
          boom_landing: "../../img/landing-promo-18-mobile.jpg",
          marusia_landing: "../../img/landing-promo-19-mobile.jpg",
          capsula_landing: "../../img/landing-promo-19-mobile.jpg",
          atom_landing: "../../img/landing-promo-20-mobile.jpg",
          apteka_landing: "../../img/landing-promo-2-mobile.jpg",
          youla_landing: "../../img/landing-promo-4-mobile.jpg",
          vkjob_landing: "../../img/landing-promo-6-mobile.jpg",
          vkclassifieds_landing: "../../img/landing-promo-3-mobile.jpg",
          mygames_landing: "../../img/landing-promo-7-mobile.jpg",
          delivery_landing: "../../img/landing-promo-8-mobile.jpg",
          citymobil_landing: "../../img/landing-promo-9-mobile.jpg",
          citydrive_landing: "../../img/landing-promo-10-mobile.jpg",
          samokat_landing: "../../img/landing-promo-11-mobile.jpg",
          skillbox_landing: "../../img/landing-promo-12-mobile.jpg",
          uchi_landing: "../../img/landing-promo-14-mobile.jpg",
          geekbrains_landing: "../../img/landing-promo-15-mobile.jpg",
          algoritmika_landing: "../../img/landing-promo-16-mobile.jpg",
          tetrika_landing: "../../img/landing-promo-17-mobile.jpg",
          skillfactory_landing: "../../img/landing-promo-13-mobile.jpg",
          contented_landing: "../../img/landing-promo-3-mobile.jpg",
          productlive_landing: "../../img/landing-promo-3-mobile.jpg",
          reco_landing: "../../img/landing-promo-3-mobile.jpg",
          remains_vk_landing: "../../img/landing-promo-3-mobile.jpg",
          remains_ok_landing: "../../img/landing-promo-3-mobile.jpg",
        }
      },
      {
        selector: ".landing-promo__bg-pic_mobile",
        attribute: "srcset",
        values: {
          "": "../../img/landing-promo-3-mobile@2x.jpg 2x",
          vk_landing: "../../img/landing-promo-3-mobile@2x.jpg 2x",
          ok_landing: "../../img/landing-promo-5-mobile@2x.jpg 2x",
          mail_landing: "../../img/landing-promo-1-mobile@2x.jpg 2x",
          boom_landing: "../../img/landing-promo-18-mobile@2x.jpg 2x",
          marusia_landing: "../../img/landing-promo-19-mobile@2x.jpg 2x",
          capsula_landing: "../../img/landing-promo-19-mobile@2x.jpg 2x",
          atom_landing: "../../img/landing-promo-20-mobile@2x.jpg 2x",
          apteka_landing: "../../img/landing-promo-2-mobile@2x.jpg 2x",
          youla_landing: "../../img/landing-promo-4-mobile@2x.jpg 2x",
          vkjob_landing: "../../img/landing-promo-6-mobile@2x.jpg 2x",
          vkclassifieds_landing: "../../img/landing-promo-3-mobile@2x.jpg 2x",
          mygames_landing: "../../img/landing-promo-7-mobile@2x.jpg 2x",
          delivery_landing: "../../img/landing-promo-8-mobile@2x.jpg 2x",
          citymobil_landing: "../../img/landing-promo-9-mobile@2x.jpg 2x",
          citydrive_landing: "../../img/landing-promo-10-mobile@2x.jpg 2x",
          samokat_landing: "../../img/landing-promo-11-mobile@2x.jpg 2x",
          skillbox_landing: "../../img/landing-promo-12-mobile@2x.jpg 2x",
          uchi_landing: "../../img/landing-promo-14-mobile@2x.jpg 2x",
          geekbrains_landing: "../../img/landing-promo-15-mobile@2x.jpg 2x",
          algoritmika_landing: "../../img/landing-promo-16-mobile@2x.jpg 2x",
          tetrika_landing: "../../img/landing-promo-17-mobile@2x.jpg 2x",
          skillfactory_landing: "../../img/landing-promo-13-mobile@2x.jpg 2x",
          contented_landing: "../../img/landing-promo-3-mobile@2x.jpg 2x",
          productlive_landing: "../../img/landing-promo-3-mobile@2x.jpg 2x",
          reco_landing: "../../img/landing-promo-3-mobile@2x.jpg 2x",
          remains_vk_landing: "../../img/landing-promo-3-mobile@2x.jpg 2x",
          remains_ok_landing: "../../img/landing-promo-3-mobile@2x.jpg 2x",
        }
      },
    ],
  };

  for (let keyParams in resultParams) {
    for (let keyList in whiteList) {
      if (keyList === keyParams) {
        whiteList[keyList].forEach((element) => {
          let selector = element.selector;
          let attribute = element.attribute;
          let value = element.values[resultParams[keyParams]] ? element.values[resultParams[keyParams]] : element.values[""];
          updateElement(selector, attribute, value);
        })
        if (keyList === 'utm_campaign') {
          let allLinks = document.querySelectorAll('a');
    
          allLinks.forEach((link) => {
            if (link.href.indexOf('trk.mail.ru') !== -1) {
              let oldHref = link.href
              if (link.href.indexOf('?') !== -1) {
                link.href = `${oldHref}&mt_sub4=${resultParams[keyParams]}`
              } else {
                link.href = `${oldHref}?mt_sub4=${resultParams[keyParams]}`
              }
            }
          })
        }
      }
    }
  }
}

window.updateUtmContent = updateUtmContent;

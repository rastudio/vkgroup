// Header mobile

let burger = document.querySelector(".burger-container");
let header = document.querySelector(".header");
let html = document.querySelector("html");

if (burger) {
  burger.addEventListener("click", () => {
    header.classList.toggle("header_menu-opened");
    html.classList.toggle("overflow-hidden");
  });
}

let headerTabBg = document.querySelector(".header__tab-bg");
let headerTab = document.querySelectorAll(".header__tab");
let headerMenuItem = document.querySelectorAll(".header__menu-item");
let headerTabWrapper = document.querySelectorAll(".header__tab-wrapper");

// Header tabs
if (screen.width > 1150) {
  headerMenuItem.forEach((item) => {
    let itemHeaderTabWrapper = item.querySelector(".header__tab-wrapper");
    let headerTab = item.querySelector(".header__tab");
    if (itemHeaderTabWrapper) {
      item.addEventListener("mouseover", () => {
        headerTabBg.classList.add("header__tab-bg_active");
        headerTab.classList.add("header__tab_active");
        itemHeaderTabWrapper.classList.add("header__tab-wrapper_active");
      });
      item.addEventListener("mouseout", () => {
        headerTabBg.classList.remove("header__tab-bg_active");
        headerTabBg.classList.remove("header__tab-bg_hovered");
        headerTab.classList.remove("header__tab_active");
        itemHeaderTabWrapper.classList.remove("header__tab-wrapper_active");
      });
    }
  });

  document.addEventListener("click", (e) => {
    if (e.target.classList.contains("header__tab-wrapper") || e.target.classList.contains("header__tab-list")) {
      headerTabBg.classList.remove("header__tab-bg_active");
      headerTabBg.classList.remove("header__tab-bg_hovered");
      headerTab.forEach((tab) => {
        tab.classList.remove("header__tab_active");
      });
      headerTabWrapper.forEach((wrapper) => {
        wrapper.classList.remove("header__tab-wrapper_active");
      });
    }
  });

  headerTabWrapper.forEach((tabWrapper) => {
    tabWrapper.addEventListener("mouseover", () => {
      headerTabBg.classList.add("header__tab-bg_hovered");
    });
  });
} else {
  let headerMenuLink = document.querySelectorAll(".header__menu-link");
  let headerTab = document.querySelectorAll(".header__tab");
  let headerWrapper = document.querySelectorAll(".header__tab-wrapper");

  headerMenuLink.forEach((link, i) => {
    link.addEventListener("click", (e) => {
      link.classList.toggle("header__menu-link_active");
      headerMenuLink.forEach((item, j) => {
        if (j != i) {
          item.classList.remove("header__menu-link_active");
        }
      });
      if (link.dataset.tab) {
        e.preventDefault();
        headerWrapper.forEach((wrapper, m) => {
          if (m == Number(link.dataset.tab)) {
            wrapper.classList.toggle("header__tab-wrapper_active");
          } else {
            wrapper.classList.remove("header__tab-wrapper_active");
          }
        });
        headerTab.forEach((tab, k) => {
          if (k == Number(link.dataset.tab)) {
            tab.classList.toggle("header__tab_active");
          } else {
            tab.classList.remove("header__tab_active");
          }
        });
      } else {
        headerWrapper.forEach((wrappers) => {
          wrappers.classList.remove("header__tab-wrapper_active");
        });
        headerTab.forEach((tabs) => {
          tabs.classList.remove("header__tab_active");
        });
      }
    });
  });

  document.addEventListener("click", (e) => {
    if (!e.target.closest(".header")) {
      headerWrapper.forEach((wrappers) => {
        wrappers.classList.remove("header__tab-wrapper_active");
      });
      headerTab.forEach((tabs) => {
        tabs.classList.remove("header__tab_active");
      });
    }
  });
}

document.addEventListener("scroll", () => {
  if (window.scrollY > 70) {
    header.classList.add("header_scroll");
  } else {
    header.classList.remove("header_scroll");
  }
});

// Кастомный курсор
let headerTabColumn = document.querySelectorAll(".header__tab-column");
let headerTabColumnIsHovered = false;
const customСursor = document.querySelector(".custom-cursor");

headerTabColumn.forEach((column) => {
  column.addEventListener("mouseenter", () => {
    headerTabColumnIsHovered = true;
  });
  column.addEventListener("mouseleave", () => {
    headerTabColumnIsHovered = false;
  });
});

const mouseMove = function (e) {
  let x = e.clientX;
  let y = e.clientY;
  customСursor.style.left = x + "px";
  customСursor.style.top = y + "px";
  if (headerTabBg.classList.contains("header__tab-bg_hovered") && !headerTabColumnIsHovered) {
    customСursor.classList.add("custom-cursor_active");
  } else {
    customСursor.classList.remove("custom-cursor_active");
  }
};

document.addEventListener("click", (e) => {
  if (e.target.classList.contains("header__tab-wrapper") || e.target.classList.contains("header__tab-list")) {
    customСursor.classList.remove("custom-cursor_active");
  }
});

document.addEventListener("mousemove", mouseMove);

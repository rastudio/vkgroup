let bigBannerNext = document.querySelector(".big-banner__next");
let bigBanner = document.querySelector(".big-banner");

if (window.innerWidth > 768) {
  bigBanner.classList.remove("is-active");
}
// let bigBannerProgressCurrent = document.querySelector(
//   ".big-banner__progress-current"
// );
// let len = Math.PI * 2 * 100;
// let autoplayDelay = 10000;
// let stopAnimation = false;

// const mainSlider = new Swiper(".big-banner__swiper", {
//   speed: 400,
//   spaceBetween: 100,
//   autoplay: {
//     delay: autoplayDelay,
//     // disableOnInteraction: false,
//     // pauseOnMouseEnter: true,
//   },
//   loop: true,
//   navigation: {
//     nextEl: ".big-banner__btn_next",
//     prevEl: ".big-banner__btn_prev",
//   },
// });

bigBannerNext.addEventListener("click", () => {
  let distance = bigBanner.clientHeight + window.pageYOffset + bigBanner.getBoundingClientRect().top;

  window.scrollTo({
    top: distance,
    behavior: "smooth",
  });
});

// bigBannerProgressCurrent.style.strokeDasharray = `${len}px`;

// function update(v) {
//   var offset = -len - (len * v) / 100;
//   bigBannerProgressCurrent.style.strokeDashoffset = `${offset}px`;
// }

// function animateProgress(duration) {
//   var requestID;
//   var startTime = null;
//   var time;

//   var animate = function (time) {
//     time = Date.now();

//     if (startTime === null) {
//       startTime = time;
//     }

//     var progress = time - startTime;

//     if (progress < duration && !stopAnimation) {
//       update((progress / duration) * 100);
//       window.requestAnimationFrame(animate);
//     }
//   };
//   window.requestAnimationFrame(animate);
// }

// mainSlider.on("autoplay", function () {
//   animateProgress(autoplayDelay);
// });

// document.addEventListener("DOMContentLoaded", () => {
//   animateProgress(autoplayDelay);
// });

// mainSlider.on("autoplayStop", function () {
//   stopAnimation = true;
//   bigBannerProgressCurrent.style.strokeDashoffset = `inherit`;
// });

let bigBannerVideo = document.querySelector(".big-banner__video");
// let bigBanner = document.querySelector(".big-banner");

if (bigBannerVideo) {
  // document.addEventListener("DOMContentLoaded", () => {
  //   // bigBannerVideo.play();
  // });
  // bigBannerVideo.addEventListener("ended", () => {
  //   bigBanner.classList.add("is-active");
  // });
}

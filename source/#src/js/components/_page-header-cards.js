if (screen.width <= 768) {
  const pageHeaderSlider = new Swiper(".page-header__cards", {
    loop: true,
    pagination: {
      el: ".swiper-pagination",
    },
  });
}

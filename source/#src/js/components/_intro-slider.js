const sliders = document.querySelectorAll('.intro-slider');

if (sliders.length) {
  sliders.forEach((slider) => {
    const introSlider = slider.querySelector('.intro-slider__wrapper');
    const introThumbsSlider = slider.querySelector('.intro-slider__thumbs-wrapper');
    const scrollbar = slider.querySelector('.intro-slider__scrollbar');
    const btnPrev = slider.querySelector('.intro-slider__btn_prev');
    const btnNext = slider.querySelector('.intro-slider__btn_next');

    const thumbsSlider = new Swiper(introThumbsSlider, {
      spaceBetween: 20,
      slidesPerView: 'auto',
      watchSlidesProgress: true,
      scrollbar: {
        el: scrollbar,
        draggable: true,
        dragSize: 125,
        dragClass: 'intro-slider__scrollbar-drag',
      },
      navigation: {
        nextEl: btnNext,
        prevEl: btnPrev,
      },
      breakpoints: {
        768: {
          slidesPerView: 4,
          scrollbar: {
            dragSize: 400,
          },
        },
      },
    });

    const mainSlider = new Swiper(introSlider, {
      loop: true,
      spaceBetween: 70,
      thumbs: {
        swiper: thumbsSlider,
      },
      mousewheel: {
        invert: true,
        forceToAxis: true,
      },
    });
  });
}


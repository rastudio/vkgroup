const projectSlider = new Swiper(".projects-swiper", {
  speed: 20000,
  slidesPerView: "auto",
  loop: true,
  // freeMode: true,
  // // spaceBetween: 30,
  autoplay: {
    delay: 0,
  },
});

// Параллакс фото
let projectsCardLargeContainers = document.querySelectorAll(".projects__card_with-photo .projects__card-pic");
let interval = 1000 / 60;

setInterval(() => {
  projectsCardLargeContainers.forEach((container) => {
    if (container.getBoundingClientRect().right > -container.clientWidth) {
      let distancePercent = 100 - ((container.getBoundingClientRect().right - container.clientWidth) / document.documentElement.clientWidth) * 100;
      let photo = container.querySelector(".projects__card-pic-img");
      if (container.closest(".projects__card_large")) {
        photo.style.transform = `translateX(${distancePercent / 2}%)`;
      }
      if (container.closest(".projects__card_medium")) {
        photo.style.transform = `translateX(${distancePercent / 3}%)`;
      }
    }
  });
}, interval);

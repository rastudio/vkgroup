let productPresentationSlider = document.querySelector(".product-presentation__slider");
let productPresentationSliderCounterActive = document.querySelector(".product-presentation__slider-counter-active");
let productPresentationSliderCounterAll = document.querySelector(".product-presentation__slider-counter-all");

if (productPresentationSlider) {
  const productPresentationSliderInit = new Swiper(productPresentationSlider, {
    navigation: {
      nextEl: ".product-presentation__slider-btn_next",
      prevEl: ".product-presentation__slider-btn_prev",
    },

    on: {
      init: function () {
        productPresentationSliderCounterAll.textContent = `${this.slides.length}`;
      },
      activeIndexChange: function () {
        productPresentationSliderCounterActive.textContent = `${this.activeIndex + 1}`;
      },
    },
  });
}

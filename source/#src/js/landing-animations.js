const screens = document.querySelectorAll("[data-animate]");
const breakpoint = window.matchMedia("(min-width:1024px)");

let elTop;
let windowHeight;

const returnAnimatePoint = (el) => {
  elTop = el.getBoundingClientRect().top;
  windowHeight = window.innerHeight;
  return windowHeight / 1.4 - elTop;
};

const trackingScreenBlock = () => {
  screens.forEach((screen) => {
    if (returnAnimatePoint(screen) > 0 && !screen.classList.contains("show")) {
      screen.classList.add("show");
    }
    if (returnAnimatePoint(screen) > windowHeight / 5 && !screen.classList.contains("show-more")) {
      screen.classList.add("show-more");
    }
  });
};

const initAnimation = () => {
  if (screens.length) {
    trackingScreenBlock();
    window.addEventListener("scroll", trackingScreenBlock);
    window.addEventListener("orientationchange", trackingScreenBlock);
  }
};

initAnimation();

let landingPageSummaryOfferTypeSpans = document.querySelectorAll(".landing-page-summary__offer-type span");
let landingPageSummaryOfferTypeArrows = document.querySelector(".landing-page-summary__offer-type svg");
let currentSpan = 0;

if (landingPageSummaryOfferTypeSpans.length) {
  setInterval(() => {
    landingPageSummaryOfferTypeArrows.classList.remove("active");
    setTimeout(() => {
      landingPageSummaryOfferTypeArrows.classList.add("active");
    }, 10);

    landingPageSummaryOfferTypeSpans[currentSpan].classList.add("active");
    landingPageSummaryOfferTypeSpans.forEach((span, i) => {
      if (i !== currentSpan) {
        span.classList.remove("active");
      }
    });
    currentSpan = currentSpan < landingPageSummaryOfferTypeSpans.length - 1 ? currentSpan + 1 : 0;
  }, 2000);
}

let landingPagePromo = document.querySelector(".landing-page-promo");
let landingPromoNext = document.querySelector(".landing-promo__next");

if (landingPromoNext) {
  landingPromoNext.addEventListener("click", () => {
    let distance = landingPagePromo.clientHeight + window.pageYOffset + landingPagePromo.getBoundingClientRect().top;

    window.scrollTo({
      top: distance,
      behavior: "smooth",
    });
  });
}

// Бесконечный скролл на втором б2с в блоке с логотипами - начало -
let landingPageChangesWrapper = document.querySelector(".landing-page-changes__wrapper");
let currentList = 1;

if (landingPageChangesWrapper) {
  landingPageChangesWrapper.addEventListener("scroll", () => {
    let landingPageChangesServices = document.querySelectorAll(".landing-page-changes__services");
    if (landingPageChangesServices[currentList].getBoundingClientRect().top < 0) {
      currentList++;
      landingPageChangesWrapper.append(landingPageChangesServices[0].cloneNode(true));
    }
  });

  setInterval(() => {
    if (document.documentElement.clientWidth > 767) {
      landingPageChangesWrapper.scrollTop += 1;
    } else {
      landingPageChangesWrapper.scrollTop += 1;
    }
  }, 2000 / 60);
}

// Бесконечный скролл на втором б2с в блоке с логотипами - конец -

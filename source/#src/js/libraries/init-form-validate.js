const formWrappers = [...document.querySelectorAll('[data-validate]')];

const resetForm = (form) => {
  setTimeout(() => {
    window.clearForm(form);
  }, 1000);
};

const baseSuccessCallback = (e) => {
  e.preventDefault();
  resetForm(e.target);
};

const baseErrorCallback = (e) => {
  e.preventDefault();
};

const subscribeFormSuccessCallback = (e) => {
  e.preventDefault();
  resetForm(e.target);

  closeModal(e.target.closest('.modal'));

  openModal(modalSubscribeSuccessMessage);
};

const eventRegistrationSuccessCallback = (e) => {
  e.preventDefault();
  resetForm(e.target);

  closeModal(e.target.closest('.modal'));

  openModal(modalRegistrationSuccessMessage);
};

const customExampleSuccessCallback = (e) => {
  e.preventDefault();
  resetForm(e.target);
  // eslint-disable-next-line no-console
  console.log('Ваша форма успешна отправлена');
};

const customExampleErrorCallback = (e) => {
  e.preventDefault();
  // eslint-disable-next-line no-console
  console.error('Отправка формы невозможна, заполните все обязательные поля');
};

const callbacks = {
  base: {
    successCallback: baseSuccessCallback,
    errorCallback: baseErrorCallback,
  },
  subscribeForm: {
    successCallback: subscribeFormSuccessCallback,
    errorCallback: baseErrorCallback,
  },
  eventRegistration: {
    successCallback: eventRegistrationSuccessCallback,
    errorCallback: baseErrorCallback,
  },
  customExample: {
    successCallback: customExampleSuccessCallback,
    errorCallback: customExampleErrorCallback,
  },
};

const initFormValidate = () => {
  if (formWrappers.length) {
    formWrappers.forEach((wrapper) => {
      let callback = wrapper.dataset.callback;
      if (!callback) {
        callback = 'base';
      }

      const formValidate = new window.FormsValidate(wrapper, callbacks[callback]);

      return formValidate.init();
    });
  }
};

initFormValidate();

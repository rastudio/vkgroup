let project_folder = "build";
let source_folder = "#src";

let path = {
  build: {
    html: project_folder + "/",
    htmlpages: project_folder + "/html/",
    css: project_folder + "/css/",
    csspages: project_folder + "/css/pages/",
    js: project_folder + "/js/",
    jspages: project_folder + "/js/pages/",
    fonts: project_folder + "/fonts/",
    files: project_folder + "/files/",
    img: project_folder + "/img/",
  },
  src: {
    html: [source_folder + "/*.html", "!" + source_folder + "/_*.html"],
    htmlpages: source_folder + "/html/**/*",
    css: source_folder + "/scss/style.scss",
    csspages: source_folder + "/scss/pages/*.scss",
    js: source_folder + "/js/script.js",
    jspages: source_folder + "/js/pages/*.js",
    fonts: source_folder + "/fonts/*",
    files: source_folder + "/files/*",
    img: source_folder + "/img/**/*.{jpg,png,svg,gif,ico,webp}",
  },
  watch: {
    html: source_folder + "/**/*.html",
    css: source_folder + "/scss/**/*.scss",
    js: source_folder + "/js/**/*.js",
    img: source_folder + "/img/**/*.{jpg,png,svg,gif,ico,webp}",
  },
  clean: "./" + project_folder + "/",
};

let { src, dest } = require("gulp"),
  gulp = require("gulp"),
  browsersync = require("browser-sync").create(),
  fileinclude = require("gulp-file-include"),
  del = require("del"),
  scss = require("gulp-sass"),
  autoprefixer = require("gulp-autoprefixer"),
  group_media = require("gulp-group-css-media-queries"),
  clean_css = require("gulp-clean-css"),
  rename = require("gulp-rename"),
  uglify = require("gulp-uglify-es").default,
  imagemin = require("gulp-imagemin");

function browserSync(params) {
  browsersync.init({
    server: {
      baseDir: "./" + project_folder + "/",
    },
    port: 3000,
    notify: false,
  });
}

function html() {
  return (
    src(path.src.html)
      .pipe(fileinclude())
      // .pipe(webphtml())
      .pipe(dest(path.build.html))
      .pipe(browsersync.stream())
  );
}

function htmlpages() {
  return (
    src(path.src.htmlpages)
      .pipe(fileinclude())
      // .pipe(webphtml())
      .pipe(dest(path.build.htmlpages))
      .pipe(browsersync.stream())
  );
}

function css() {
  return src(path.src.css)
    .pipe(
      scss({
        outputStyle: "expanded",
      })
    )
    .pipe(group_media())
    .pipe(
      autoprefixer({
        overrideBrowserslist: ["last 5 versions"],
        cascade: true,
      })
    )
    .pipe(dest(path.build.css))
    .pipe(
      clean_css({
        level: 2,
      })
    )
    .pipe(
      rename({
        extname: ".min.css",
      })
    )
    .pipe(dest(path.build.css))
    .pipe(browsersync.stream());
}

function csspages() {
  return src(path.src.csspages)
    .pipe(
      scss({
        outputStyle: "expanded",
      })
    )
    .pipe(group_media())
    .pipe(
      autoprefixer({
        overrideBrowserslist: ["last 5 versions"],
        cascade: true,
      })
    )
    .pipe(dest(path.build.csspages))
    .pipe(
      clean_css({
        level: 2,
      })
    )
    .pipe(
      rename({
        extname: ".min.css",
      })
    )
    .pipe(dest(path.build.csspages))
    .pipe(browsersync.stream());
}

function preloaderCss() {
  return src(source_folder + "/scss/preloader.scss")
    .pipe(
      scss({
        outputStyle: "expanded",
      })
    )
    .pipe(group_media())
    .pipe(
      autoprefixer({
        overrideBrowserslist: ["last 5 versions"],
        cascade: true,
      })
    )
    .pipe(dest(path.build.css))
    .pipe(
      clean_css({
        level: 2,
      })
    )
    .pipe(
      rename({
        extname: ".min.css",
      })
    )
    .pipe(dest(path.build.css))
    .pipe(browsersync.stream());
}

function js() {
  return src(path.src.js)
    .pipe(fileinclude())
    .pipe(dest(path.build.js))
    .pipe(uglify())
    .pipe(
      rename({
        extname: ".min.js",
      })
    )
    .pipe(dest(path.build.js))
    .pipe(browsersync.stream());
}

function preloaderJs() {
  return src(source_folder + "/js/preloader.js")
    .pipe(fileinclude())
    .pipe(dest(path.build.js))
    .pipe(uglify())
    .pipe(
      rename({
        extname: ".min.js",
      })
    )
    .pipe(dest(path.build.js))
    .pipe(browsersync.stream());
}

function landingPreloaderJs() {
  return src(source_folder + "/js/landing-preloader.js")
    .pipe(fileinclude())
    .pipe(dest(path.build.js))
    .pipe(uglify())
    .pipe(
      rename({
        extname: ".min.js",
      })
    )
    .pipe(dest(path.build.js))
    .pipe(browsersync.stream());
}

function utmJs() {
  return src(source_folder + "/js/update-utm-content.js")
    .pipe(fileinclude())
    .pipe(dest(path.build.js))
    .pipe(uglify())
    .pipe(
      rename({
        extname: ".min.js",
      })
    )
    .pipe(dest(path.build.js))
    .pipe(browsersync.stream());
}

function jspages() {
  return src(path.src.jspages)
    .pipe(fileinclude())
    .pipe(dest(path.build.jspages))
    .pipe(uglify())
    .pipe(
      rename({
        extname: ".min.js",
      })
    )
    .pipe(dest(path.build.jspages))
    .pipe(browsersync.stream());
}

function images() {
  return src(path.src.img)
    .pipe(dest(path.build.img))
    .pipe(src(path.src.img))
    .pipe(
      imagemin({
        progressive: true,
        svgoPlugins: [{ removeViewBox: false }],
        interlaced: true,
        optimizationLevel: 3, // 0 to 7
      })
    )
    .pipe(dest(path.build.img))
    .pipe(browsersync.stream());
}

function fonts() {
  return src(path.src.fonts).pipe(dest(path.build.fonts));
}

function files() {
  return src(path.src.files).pipe(dest(path.build.files));
}

function watchFiles(params) {
  gulp.watch([path.watch.html], html);
  gulp.watch([path.watch.html], htmlpages);
  gulp.watch([path.watch.css], css);
  gulp.watch([path.watch.css], csspages);
  gulp.watch([path.watch.js], js);
  gulp.watch([path.watch.js], jspages);
  gulp.watch([path.watch.img], images);
}

function clean(params) {
  return del(path.clean);
}

let build = gulp.series(clean, gulp.parallel(js, jspages, css, preloaderCss, preloaderJs, utmJs, landingPreloaderJs, csspages, html, htmlpages, images, fonts, files));
let watch = gulp.parallel(build, watchFiles, browserSync);

exports.fonts = fonts;
exports.files = files;
exports.images = images;
exports.js = js;
exports.jspages = jspages;
exports.preloaderCss = preloaderCss;
exports.preloaderJs = preloaderJs;
exports.utmJs = utmJs;
exports.landingPreloaderJs = landingPreloaderJs;
exports.css = css;
exports.csspages = csspages;
exports.html = html;
exports.htmlpages = htmlpages;
exports.build = build;
exports.watch = watch;
exports.default = watch;
